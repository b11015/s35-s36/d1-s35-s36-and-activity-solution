
const express = require('express');

// Router method - allows access to HTTP methods
const router = express.Router();

const taskControllers = require('../controllers/taskControllers');

console.log(taskControllers);

// create task route
router.post('/', taskControllers.createTaskControllers);

// Get all tasks route
router.get('/', taskControllers.getAllTasksController);

// Get single task
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController );

// Update Task Status
router.put('/updateTask/:id', taskControllers.updateTaskStatusController)

module.exports = router;