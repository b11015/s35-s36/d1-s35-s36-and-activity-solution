const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers');

console.log(userControllers);

router.post('/', userControllers.createUserControllers)

router.get('/', userControllers.getAllUsersController)

router.get('/getSingleUser/:id', userControllers.getSingleUserController)

router.put('/updateUser/:id', userControllers.updateUserIdController)

module.exports = router;