const mongoose = require('mongoose');

// Schema- blueprint for out data / document

const taskSchema = new mongoose.Schema({

	name: String,

	status: String
});

// Mongoose Model - Partner of Schema
/*
	Syntax:
		mongoose.model(<nameOfCollectionsInAtlas>, <schemaToFollow>)
*/

// Model is for manipulation objects the data or documents
module.exports = mongoose.model("Task", taskSchema);

