// required modules
const express = require('express');

/*
	it allows to connect in our MongoDB
*/
const mongoose = require('mongoose');

// port
const port = 4000

// server
 const app = express();

 // mongoose connection
 mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.gw82m.mongodb.net/tasks182?retryWrites=true&w=majority", 
 	{
 		useNewUrlParser: true,
 		useUnifiedTopology: true
 	}
 );

// this will create a notifications if the db connection is successful or not
// NOTIFICATIONS
let db = mongoose.connection

db.on('error', console.error.bind(console, "DB Connection Error"));
db.once('open', () => console.log("Successfully connected to MondgoDB"));

// middleware
 app.use(express.json());

// reading of data forms
// usually string or array are being accepted, with this middleware, this will enable us to acccept other data types
 app.use(express.urlencoded({extended: true}));

// Routes
const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks', taskRoutes);

 // port listener
 app.listen(port, () => console.log(`Server is running at port ${port}`));